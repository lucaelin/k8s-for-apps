import express from 'express';
import viewcount from './viewcount.js';
import viewhistory from './viewhistory.js';

export const app = express();
app.use(express.json());

app.get('/', (req, res) => {
  res.send({
    message: `Hello ${process.env.HELLO_NAME || 'you'}!`,
  });
})

app.get('/healthz', async (req, res) => {
  res.send({
    uptime: process.uptime(),
    message: 'OK',
    timestamp: Date.now()
  });
})

app.use('/viewcount', viewcount);
app.use('/viewhistory', viewhistory);

const port = parseInt(process.env.PORT || '3000');
app.listen(port, () => {
  console.log('Server started on port '+port);
})
