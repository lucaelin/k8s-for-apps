import { RedisClientType, createClient } from 'redis';

let client: RedisClientType;
export async function getClient() {
  if (client) return client;
   
  const redisHost = process.env.REDIS_HOST || 'localhost';
  const redisPort = process.env.REDIS_PORT || 6379;
  const redisPassword = process.env.REDIS_PASSWORD || 'password';

  client = createClient({
    url: `redis://:${redisPassword}@${redisHost}:${redisPort}`,
    legacyMode: false
  });
  client.on('error', err => console.log('Redis Client Error', err));
  await client.connect();

  return client;
}