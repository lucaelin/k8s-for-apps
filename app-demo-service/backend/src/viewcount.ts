import { Router } from 'express';
import { getClient } from './redis.js';

const router = Router();

router.get('/', async (req, res) => {
  const client = await getClient();
  const count = await client.INCR('viewcount');
  res.send({
    viewcount: count,
  });
})

export default router;