import { Router } from 'express';
import { getClient } from './redis.js';

const router = Router();

router.get('/snapshot', async (req, res) => {
  const client = await getClient();
  const count = await client.get('viewcount') ?? "0";
  await client.LPUSH('viewhistory', count);
  res.send({
    viewcount: count,
  });
})
router.get('/show', async (req, res) => {
  const client = await getClient();
  const history = await client.lRange('viewhistory', 0, -1);
  res.send({
    viewhistory: history,
  });
})

export default router;