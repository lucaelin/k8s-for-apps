#!/usr/bin/env bash

CURRENT_DIR=$(dirname $0)
echo running in $CURRENT_DIR

echo building backend
docker buildx build -t app-demo-service-backend:local -o type=oci,dest=$CURRENT_DIR/../app-demo-service-backend.tar $CURRENT_DIR/backend
microk8s ctr image import $CURRENT_DIR/../app-demo-service-backend.tar

echo building frontend
docker buildx build -t app-demo-service-frontend:local -o type=oci,dest=$CURRENT_DIR/../app-demo-service-frontend.tar $CURRENT_DIR/frontend
microk8s ctr image import $CURRENT_DIR/../app-demo-service-frontend.tar

echo building chart
microk8s helm package $CURRENT_DIR/chart
mv ./app-demo-service-*.tgz $CURRENT_DIR/../app-demo-service.tgz