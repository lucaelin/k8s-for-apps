#!/usr/bin/env sh

echo logging in
docker login registry.gitlab.com -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN
helm registry login registry.gitlab.com -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN

echo preparing buildx
docker context create builder
docker buildx create builder --use

echo building backend
docker buildx build --push --platform linux/arm64,linux/amd64 -t registry.gitlab.com/lucaelin/k8s-for-apps/app-demo-service/backend:v0.0.0-${CI_COMMIT_REF_NAME} ./backend

echo building frontend
docker buildx build --push --platform linux/arm64,linux/amd64 -t registry.gitlab.com/lucaelin/k8s-for-apps/app-demo-service/frontend:v0.0.0-${CI_COMMIT_REF_NAME} ./frontend

echo building chart
helm package ./chart --app-version v0.0.0-${CI_COMMIT_REF_NAME} --version v0.0.0-${CI_COMMIT_REF_NAME}
helm push ./app-demo-service*.tgz oci://registry.gitlab.com/lucaelin/k8s-for-apps/app-demo-service
rm app-demo-service*.tgz