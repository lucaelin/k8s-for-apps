import resolve from '@rollup/plugin-node-resolve';
import path from 'path';
import esbuild from 'rollup-plugin-esbuild';
import {rollupPluginHTML} from '@web/rollup-plugin-html';

const outputDir = 'build/';

export default {
	input: path.resolve('.', './src/index.html'),
	output: {
		dir: outputDir,
		format: 'esm',
		sourcemap: true,
	},
	perf: true,
	preserveSymlinks: false,
	preserveEntrySignatures: false,
	inlineDynamicImports: false,
	plugins: [
		rollupPluginHTML(),
		resolve({
			browser: true,
			extensions: ['.js', '.mjs', '.ts', '.json'],
		}),
		esbuild({
			minify: true,
			sourcemap: true,
			target: 'es2020',
			loaders: {
				'.js': 'js',
				'.mjs': 'js',
				'.ts': 'ts',
				'.json': 'json',
			},
		}),
	],
};
