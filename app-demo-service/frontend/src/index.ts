import {html, LitElement} from 'lit';
import { customElement, state, property } from 'lit/decorators.js';

@customElement('app-demo-service')
export class AppDemoService extends LitElement {
  @property()
  public greeting = 'Hello you!'; 
  @state()
  private viewcount = -1; 

  firstUpdated() {
    fetch('./api')
      .then(res=>res.json())
      .then(({message})=>this.greeting = message);

    fetch('./api/viewcount')
      .then(res=>res.json())
      .then(({viewcount})=>this.viewcount = viewcount);
  }

  render() {
    return html`
      <h1>${this.greeting}</h1>
      <p>${this.viewcount} visitors have visited us today!</p>
    `;
  }
}