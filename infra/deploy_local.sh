#!/usr/bin/env bash

CURRENT_DIR=$(dirname $0)

microk8s helm upgrade -f $CURRENT_DIR/local.values.yaml --install app-demo-local -n app-demo-local --create-namespace $CURRENT_DIR/../app-demo-service.tgz --wait