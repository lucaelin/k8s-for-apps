#!/usr/bin/env bash

CURRENT_DIR=$(dirname $0)

#microk8s helm pull --debug oci://registry.gitlab.com/lucaelin/k8s-for-apps/app-demo-service/app-demo-service --version v0.0.0-main
microk8s helm upgrade -f $CURRENT_DIR/prod.values.yaml --install app-demo-prod -n app-demo-prod --create-namespace oci://registry.gitlab.com/lucaelin/k8s-for-apps/app-demo-service/app-demo-service --version v0.0.0-main --wait