#!/usr/bin/env bash

microk8s uninstall
multipass delete microk8s-vm
multipass purge

# install microk8s
brew install ubuntu/microk8s/microk8s
microk8s install --cpu=6 -y
microk8s status --wait-ready

# install extensions
microk8s enable community
microk8s enable linkerd
microk8s enable metrics-server
#microk8s enable registry:size=40Gi
microk8s enable metallb:192.168.31.1-192.168.31.254
sudo route -nv add -net 192.168.31.0/24 -interface bridge100

# prepare workspace
# multipass exec microk8s-vm -- sudo bash
multipass mount $(pwd) microk8s-vm

# test cluster
microk8s kubectl get pods --all-namespaces

# install gw-api
microk8s kubectl apply -f https://github.com/kubernetes-sigs/gateway-api/releases/download/v1.0.0/standard-install.yaml
microk8s helm install eg oci://docker.io/envoyproxy/gateway-helm --version v0.0.0-latest -n envoy-gateway-system --create-namespace
microk8s kubectl wait --timeout=5m -n envoy-gateway-system deployment/envoy-gateway --for=condition=Available

# test gw-api
microk8s kubectl apply -f https://github.com/envoyproxy/gateway/releases/download/latest/quickstart.yaml -n envoy-gateway-system
microk8s kubectl wait --timeout=5m -n envoy-gateway-system deployment/backend  --for=condition=Available
export ENVOY_SERVICE=$(microk8s kubectl get svc -n envoy-gateway-system --selector=gateway.envoyproxy.io/owning-gateway-name=eg -o jsonpath='{.items[0].metadata.name}')
export GATEWAY_HOST=$(microk8s kubectl get svc/${ENVOY_SERVICE} -n envoy-gateway-system -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
curl --verbose --header "Host: www.example.com" http://$GATEWAY_HOST/get
