# configure microk8s
```sh
brew install ubuntu/microk8s/microk8s
microk8s install -y
microk8s status --wait-ready

microk8s enable community
microk8s enable linkerd
#microk8s enable registry:size=40Gi
microk8s enable metallb:192.168.31.1-192.168.31.254

# multipass exec microk8s-vm -- sudo bash
git clone git@gitlab.com:lucaelin/k8s-for-apps.git
cd k8s-for-apps
multipass mount . microk8s-vm
```

# inside vm
```sh
alias kubectl="microk8s kubectl"
alias helm="microk8s helm"
alias curl="curl --interface bridge100" # macos...
kubectl get pods --all-namespaces
kubectl apply -f https://github.com/kubernetes-sigs/gateway-api/releases/download/v1.0.0/standard-install.yaml
```

# test gw-api
```sh
helm install eg oci://docker.io/envoyproxy/gateway-helm --version v0.0.0-latest -n envoy-gateway-system --create-namespace
kubectl wait --timeout=5m -n envoy-gateway-system deployment/envoy-gateway --for=condition=Available
kubectl apply -f https://github.com/envoyproxy/gateway/releases/download/latest/quickstart.yaml -n default
export ENVOY_SERVICE=$(kubectl get svc -n envoy-gateway-system --selector=gateway.envoyproxy.io/owning-gateway-namespace=default,gateway.envoyproxy.io/owning-gateway-name=eg -o jsonpath='{.items[0].metadata.name}')
export GATEWAY_HOST=$(kubectl get svc/${ENVOY_SERVICE} -n envoy-gateway-system -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
curl --verbose --header "Host: www.example.com" http://$GATEWAY_HOST/get
```

# chart installation
```sh
helm install my-app -n my-app --create-namespace --set myvalue=foobar $(pwd)/charts
helm upgrade my-app -n my-app --set myvalue=foobar $(pwd)/charts
``` 

# shutdown
```sh
multipass stop microk8s-vm
multipass delete microk8s-vm
multipass purge

microk8s stop
microk8s uninstall
brew uninstall ubuntu/microk8s/microk8s
```


```
install script
ssh instance
slides
tasks definieren 
 - redis view count
 - kafka something 
 - user auth (https://linkerd.io/2.15/tasks/configuring-per-route-policy/#creating-per-route-policy-resources)
 - aws resources (https://aws-controllers-k8s.github.io/community/docs/tutorials/rds-example/)
service mesh


WASM
 - https://github.com/KWasm/kwasm-operator

VMs

Networking
 - metallb arp
 - iptables
 - ebpf

CRDs and Controllers
 - 

CI/CD
 - Flux
 - GitLab
```